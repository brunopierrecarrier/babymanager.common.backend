package innmov.common.backend;

/**
 * Created by bruno on 2017-09-28.
 */
public class ThreadContext {

    private static final ThreadLocal<Throwable> exceptionHolder = new ThreadLocal();
    private static final ThreadLocal<Object> extraInfo = new ThreadLocal();
    private static final ThreadLocal<Integer> remoteServerErrorCode = new ThreadLocal();
    private static final ThreadLocal<String> remoteServerResponseBody = new ThreadLocal();

    public static ThreadLocal<Throwable> getExceptionHolder() {
        return exceptionHolder;
    }

    public static void reset() {
        exceptionHolder.remove();
        extraInfo.remove();
        remoteServerErrorCode.remove();
        remoteServerResponseBody.remove();
    }

    public static Throwable getException() {
        return exceptionHolder.get();
    }
    public static Integer getRemoteServerErrorCode() { return remoteServerErrorCode.get();}
    public static String getRemoteServerResponseBody() { return remoteServerResponseBody.get();}
    public static Object getExtraInfo() { return extraInfo.get();}

    public static void setExtraInfo(Object object) {
        extraInfo.set(object);
    }

    public static void setRemoteServerResponseBody(String responseBody) {
        remoteServerResponseBody.set(responseBody);
    }
    public static void setRemoteServerErrorCode(Integer responseCode) {
        remoteServerErrorCode.set(responseCode);
    }

    public static void setThrowable(Throwable throwable) {
        exceptionHolder.set(throwable);
    }
}
