package innmov.common.backend;

import java.util.HashMap;
import java.util.Map;

public class SlackClient {

    public static final String MAIN_SERVER_ERRORS_CHANNEL = "https://hooks.slack.com/services/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";
    public static final String SERVER_ERRORS_LESS_RELEVANT = "https://hooks.slack.com/services/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";
    public static final String DEBUG_MESSAGE_CHANNEL = "https://hooks.slack.com/services/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";
    public static final String PUSH_NOTIF_ERRORS = "https://hooks.slack.com/services/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";
    public static final String ANDROID_DEBUG_URL = "https://hooks.slack.com/services/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";

    BabyManagerHttpClient httpClient;

    public SlackClient(BabyManagerHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public void reportBug(String bugReport, int appVersion, boolean android) {
        try {
            Map<String, String> body = new HashMap<>();
            body.put("text", makeSlackErrorMessage(bugReport));

            String url = ANDROID_DEBUG_URL;

            String response = httpClient.postForObject(url, body, String.class);
        } catch (Exception exception) {

        }

    }

    private String makeSlackErrorMessage(String errorMessage) {
        return String.format(":thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: :thunder_cloud_and_rain: \n \n "
                + errorMessage);
    }

    public void reportServerException(String message) {
        try {
            Map<String, String> body = new HashMap<>();
            body.put("text", makeSlackErrorMessage(message));

            String url = getSlackUrlForServerException(message);
            String response = httpClient.postForObject(url, body, String.class);
        } catch (Exception newException) {

        }
    }

    public void debugMessage(String message) {
        try {
            Map<String, String> body = new HashMap<>();
            body.put("text", makeSlackErrorMessage(message));

            String url = DEBUG_MESSAGE_CHANNEL;
            String response = httpClient.postForObject(url, body, String.class);
        } catch (Exception newException) {

        }
    }

    public void reportPushNotifError(String message) {
        try {
            Map<String, String> body = new HashMap<>();
            body.put("text", makeSlackErrorMessage(message));

            String url = PUSH_NOTIF_ERRORS;
            String response = httpClient.postForObject(url, body, String.class);
        } catch (Exception newException) {

        }
    }

    private String getSlackUrlForServerException(String message) {
        if (message.contains("/authentication/babyUniqueIdentifier")
                || message.contains("host: 192.168.")
                ) {
            return SERVER_ERRORS_LESS_RELEVANT;
        } else {
            return MAIN_SERVER_ERRORS_CHANNEL;
        }
    }
}
