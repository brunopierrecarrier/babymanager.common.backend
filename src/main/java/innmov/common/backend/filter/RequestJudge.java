package innmov.common.backend.filter;

import javax.servlet.http.HttpServletResponse;

public class RequestJudge {

    public boolean isBenignRaceCondition(String path, String extraInfo) {
        return extraInfo.toLowerCase().contains("Duplicate entry".toLowerCase())
                && pathIsBenign(path);
    }

    private boolean pathIsBenign(String path) {
        return path.toLowerCase().contains("/SaveBabyEvents".toLowerCase())
                || path.toLowerCase().contains("/user/makeBabyRelationship".toLowerCase());
    }

    public boolean requestShouldBeIgnored(String path, String requestMethod) {
        return path.toLowerCase().equals("/")
                && requestMethod.toLowerCase().equals("post");
    }

    public boolean responseShowsItShouldBeReported(HttpServletResponse httpServletResponse, String requestMethod) {
        return !requestMethod.toLowerCase().contains("head");
    }

    public boolean requestShouldBeReported(HttpServletResponse httpServletResponse, String requestMethod, Throwable exception) {
        return exception != null
                && responseShowsItShouldBeReported(httpServletResponse, requestMethod);
    }

}
