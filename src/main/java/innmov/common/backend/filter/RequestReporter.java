package innmov.common.backend.filter;

import com.google.common.collect.Sets;
import innmov.common.backend.SlackClient;
import innmov.common.backend.ThreadContext;
import innmov.common.backend.exception.ExceptionPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Enumeration;

/**
 * Created by bruno on 2018-01-01.
 */
public class RequestReporter {

    public static final String LINE_BREAK = "\n";
    private final SlackClient slackClient;
    private final ExceptionPrinter exceptionPrinter;
    private final RequestJudge requestJudge;
    private final String microserviceName;

    private static final Logger log = LoggerFactory.getLogger(RequestLogFilter.class);

    public RequestReporter(SlackClient slackClient, ExceptionPrinter exceptionPrinter, RequestJudge requestJudge, String microserviceName) {
        this.slackClient = slackClient;
        this.exceptionPrinter = exceptionPrinter;
        this.requestJudge = requestJudge;
        this.microserviceName = microserviceName;
    }

    private static final Collection<String> reportedHeaders = Sets.newHashSet("authorization", "platform", "host", "AppVersion".toLowerCase(), "X-Forwarded-For".toLowerCase());

    public void reportError(HttpServletRequest request,
                             String path,
                             String requestMethod,
                             ContentCachingRequestWrapper requestWrapper,
                             Throwable exception,
                             HttpServletResponse response
    ) throws UnsupportedEncodingException {
        String contentType = request.getHeader("Content-Type");
        String requestData = "";
        int responseStatus = response.getStatus();
        boolean applicationJsonContentType = contentType != null && contentType.toLowerCase().equals("application/json");

        if (applicationJsonContentType || !path.toLowerCase().contains("picture")) {
            requestData = getRequestData(requestWrapper);
        }

        StringBuilder stringBuilder = new StringBuilder(String.format(
                "*Microservice*: %s *Request path*: %s *Request method*: %s *Request Headers*: %s  *Response code*: %s  *User Request body*: %s " ,
                microserviceName + LINE_BREAK,
                path + LINE_BREAK,
                requestMethod + LINE_BREAK,
                extractHeaders(request) + LINE_BREAK,
                responseStatus + LINE_BREAK,
                requestData + LINE_BREAK + LINE_BREAK
        ));

        if (ThreadContext.getRemoteServerResponseBody() != null) {
            if (ThreadContext.getRemoteServerResponseBody().contains("Nested cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry")) {
                return;
            }

            stringBuilder.append("*Remote server response body*: " + ThreadContext.getRemoteServerResponseBody() + LINE_BREAK);
        }

        if (ThreadContext.getRemoteServerResponseBody() != null) {
            stringBuilder.append("*Extra info*: " + ThreadContext.getExtraInfo().toString() + LINE_BREAK + LINE_BREAK);
        }

        if (ThreadContext.getRemoteServerErrorCode() != null) {
            stringBuilder.append("*Remote server status code*: " + ThreadContext.getRemoteServerErrorCode() + LINE_BREAK);
        }

        if (exception != null
                && exception.getMessage() != null
                && !exception.getMessage().contains("LockAcquisitionException")
                && !exception.getMessage().contains("java.io.EOFException")
                && !exception.getMessage().contains("Unexpected EOF read on the socket")
                ) {

            String extraInfo = (exception.getCause() != null && exception.getCause().getCause() != null) ? exception.getCause().getCause().toString() : "";
            if (requestJudge.isBenignRaceCondition(path, extraInfo)) {
                return;
            }


            String stackTrace;
            stackTrace = exceptionPrinter.printStackTrace(exception, true);

            stringBuilder.append(String.format(
                    "*Exception message*: %s *Exception cause*: %s *Nested cause:* %s *Stack trace*: %s " ,
                    String.valueOf(exception.getMessage()) + LINE_BREAK,
                    String.valueOf(exception.getCause()) + LINE_BREAK,
                    extraInfo + LINE_BREAK + LINE_BREAK,
                    stackTrace
            ));

            slackClient.reportServerException(stringBuilder.toString());
            log.error(String.format("Error reported to Slack: %s ", stackTrace));
        } else if (exception == null) {

            slackClient.reportServerException(stringBuilder.toString());
            log.error("Error reported to Slack: %s", exceptionPrinter.printStackTrace(exception, true));
        }
    }


    private String getRequestData(final HttpServletRequest request) throws UnsupportedEncodingException {
        String payload = null;
        ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        if (wrapper != null) {
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
            }
        }
        return payload;
    }


    private String extractHeaders(HttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder();

        Enumeration<String> headerNames = request.getHeaderNames();

        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();

            if (reportedHeaders.contains(headerName.toLowerCase())  || request.getServletPath().contains("authentication")) {
                stringBuilder.append(String.format("%s: %s %s", headerName, request.getHeader(headerName), LINE_BREAK));
            }
        }

        stringBuilder.append(LINE_BREAK);
        return stringBuilder.toString();
    }

}
