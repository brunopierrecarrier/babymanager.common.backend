package innmov.common.backend.filter.metrics;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.model.*;
import innmov.common.backend.filter.SimpleFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

/**
 * Created by bruno on 2018-04-13.
 */
public class StatusCodeMetricsFilter extends SimpleFilter {

    private final MetricsReporter metricsReporter;
    private final ExecutorService executorService;

    public StatusCodeMetricsFilter(MetricsReporter metricsReporter, ExecutorService executorService) {
        this.metricsReporter = metricsReporter;
        this.executorService = executorService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            chain.doFilter(request, response);
        } finally {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            HttpServletRequest httpRequest = (HttpServletRequest) request;

            String path = httpRequest.getServletPath() == null ? "" : httpRequest.getServletPath();
            int status = httpResponse.getStatus();

            if (status >= 500) {
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
//                        metricsReporter.reportHttpStatusCode(status);
//            metricsReporter.reportHttpStatusCodeCategory(status);
//            metricsReporter.reportHttpStatusCodeCategoryForPath(status, path, httpRequest.getMethod());
                        metricsReporter.reportHttpStatusCodeForPath(status, path, httpRequest.getMethod());
                    }
                });
            }

        }

    }
}
