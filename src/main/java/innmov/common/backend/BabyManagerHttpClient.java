package innmov.common.backend;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.*;

import java.net.URI;
import java.util.Map;

/**
 * Created by bruno on 2017-10-27.
 */
public class BabyManagerHttpClient {

    private final RestTemplate restTemplate;

    public BabyManagerHttpClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public <T> T getForObject(String url, Class<T> responseType, Object... uriVariables) throws Exception {
        try {
            return restTemplate.getForObject(url, responseType, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> T getForObject(String url, Class<T> responseType, Map<String, ?> uriVariables) throws Exception {
        try {
            return restTemplate.getForObject(url, responseType, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> T getForObject(URI url, Class<T> responseType) throws Exception {
        try {
            return restTemplate.getForObject(url, responseType);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> T postForObject(String url, Object request, Class<T> responseType, Object... uriVariables) throws Exception {
        try {
            return restTemplate.postForObject(url, request, responseType, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }

    private void saveErrorForReporting(Object url, Exception exception) {
        if (exception instanceof HttpServerErrorException) {
            HttpServerErrorException httpServerErrorException = (HttpServerErrorException) exception;
            ThreadContext.setRemoteServerErrorCode(httpServerErrorException.getStatusCode().value());
            ThreadContext.setRemoteServerResponseBody(httpServerErrorException.getResponseBodyAsString());
        }

        ThreadContext.setExtraInfo(url);
        ThreadContext.setThrowable(exception);
    }


    public <T> T postForObject(String url, Object request, Class<T> responseType, Map<String, ?> uriVariables) throws Exception {
        try {
            return restTemplate.postForObject(url, request, responseType, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> T postForObject(URI url, Object request, Class<T> responseType) throws Exception {
        try {
            return restTemplate.postForObject(url, request, responseType);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public void delete(String url, Object... uriVariables) throws Exception {
        try {
            restTemplate.delete(url, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public void delete(String url, Map<String, ?> uriVariables) throws Exception {
        try {
            restTemplate.delete(url, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public void delete(URI url) throws Exception {
        try {
            restTemplate.delete(url);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }

    public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<T> responseType, Object... uriVariables) throws Exception {
        try {
            return restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<T> responseType, Map<String, ?> uriVariables) throws Exception {
        try {
            return restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> ResponseEntity<T> exchange(URI url, HttpMethod method, HttpEntity<?> requestEntity, Class<T> responseType) throws Exception {
        try {
            return restTemplate.exchange(url, method, requestEntity, responseType);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType, Object... uriVariables) throws Exception {
        try {
            return restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType, Map<String, ?> uriVariables) throws Exception {
        try {
            return restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }


    public <T> ResponseEntity<T> exchange(URI url, HttpMethod method, HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType) throws Exception {
        try {
            return restTemplate.exchange(url, method, requestEntity, responseType);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }



    public Object postForEntity(String url, Map<String, Object> body, Class responseType) throws Exception {
        try {
            return restTemplate.postForEntity(url, body, responseType);
        } catch (Exception exception) {
            saveErrorForReporting(url, exception);
            throw new Exception(exception);
        }
    }
}
