package innmov.common.backend;

import com.amazonaws.services.cloudwatch.AmazonCloudWatchAsyncClientBuilder;
import innmov.common.backend.filter.metrics.MetricsReporter;
import org.junit.Test;

/**
 * Created by bruno on 2018-04-15.
 */
public class General {

//    @Test
    public void canBatch() {
        MetricsReporter metricsReporter = new MetricsReporter(AmazonCloudWatchAsyncClientBuilder.standard().withRegion("eu-west-1").build(), "test-suite");

        for (int i = 0; i < 51; i++) {
            metricsReporter.reportHttpStatusCode(123);
        }

    }
}
