package innmov.common.backend.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Bruno Carrier
 *         brunopierrecarrier@gmail.com
 *         10/25/2016
 */

public class RequestLogFilter extends SimpleFilter {

    private static final Logger log = LoggerFactory.getLogger(RequestLogFilter.class);

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        String path = request.getServletPath() == null ? "" : request.getServletPath();
        String authorization = request.getHeader("Authorization") == null ? "" : request.getHeader("Authorization");
        long start = System.currentTimeMillis();
        if (shouldLog(path)) {
            log.info("$$ Request started for: {} with token []"
                    .replace("$$", request.getMethod())
                    .replace("{}", path)
                    .replace("[]", authorization));
        }
        chain.doFilter(req, res);
        long end = System.currentTimeMillis();
        long duration = end - start;

        HttpServletResponse httpServletResponse = (HttpServletResponse) res;
        int status = httpServletResponse.getStatus();

        String message = "(Code: **) (%%) $$ Request ended for: {} with token []"
                .replace("**", String.valueOf(status))
                .replace("%%", duration > 10000 ? duration / 1000 + " s" : duration + " ms")
                .replace("$$", request.getMethod())
                .replace("{}", path)
                .replace("[]", authorization);

        conditionallyLog(path, status, message);
    }

    private void conditionallyLog(String path, int status, String message) {
        if (shouldLog(path)) {
            if (status > 400 && status != 417 && status != 426) { // TODO - modify that so that the request judge handles this
                log.error(message);
            } else {
                log.info(message);
            }
        }
    }

    private boolean shouldLog(String path) {
        return !"/healthCheck".equals(path);
    }

}
