package innmov.common.backend.filter;

import innmov.common.backend.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author Bruno Carrier
 *         brunopierrecarrier@gmail.com
 *         10/25/2016
 */

public class ExceptionReportFilter extends SimpleFilter {

    private final RequestJudge requestJudge;
    private final RequestReporter requestReporter;

    public ExceptionReportFilter(RequestJudge requestJudge, RequestReporter requestReporter) {
        this.requestJudge = requestJudge;
        this.requestReporter = requestReporter;
    }

    private static final Logger log = Logger.getLogger(ExceptionReportFilter.class.getSimpleName());

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse httpServletResponse = (HttpServletResponse) res;
        String path = request.getServletPath() == null ? "" : request.getServletPath();
        String requestMethod = request.getMethod();

        ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);

        try {
            chain.doFilter(requestWrapper, res);
        } catch (Exception exception) {
            mirrorTheErrorStatusCode((HttpServletResponse) res);

            if (ThreadContext.getException() == null) {
                ThreadContext.setThrowable(exception);
            }

            throw exception;
        } finally {
            Throwable reportedException = ThreadContext.getException();

            if (requestJudge.requestShouldBeReported(httpServletResponse, requestMethod, reportedException)
                    && !requestJudge.requestShouldBeIgnored(path, requestMethod)
                    ) {
                requestReporter.reportError(request, path, requestMethod, requestWrapper, reportedException, httpServletResponse);
            }

            ThreadContext.reset();
        }
    }

    private void mirrorTheErrorStatusCode(HttpServletResponse res) {
        if (ThreadContext.getRemoteServerErrorCode() != null) {
            res.setStatus(ThreadContext.getRemoteServerErrorCode());
        }
    }

}
