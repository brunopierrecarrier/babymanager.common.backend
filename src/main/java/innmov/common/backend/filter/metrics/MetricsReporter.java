package innmov.common.backend.filter.metrics;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by bruno on 2018-04-13.
 */
public class MetricsReporter {


    public static final int MAX_DATUM_COLLECTION_SIZE = 20;
    private final AmazonCloudWatch amazonCloudWatch;
    private final String serviceName;

    public MetricsReporter(AmazonCloudWatch amazonCloudWatch, String serviceName) {
        this.amazonCloudWatch = amazonCloudWatch;
        this.serviceName = serviceName;
    }

    // See https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/cloudwatch_concepts.html
    public void reportHttpStatusCodeForPath(int statusCode, String requestPath, String requestMethod) {
        MetricDatum datum = new MetricDatum()
                .withMetricName(String.format("Path %s (%s) Http Status %s", requestPath, requestMethod, statusCode))
                .withUnit(StandardUnit.Count)
                .withValue(1d)
                .withDimensions(getDimension());

        sendMetrics(datum);
    }

    public void reportHttpStatusCode(int statusCode) {
        MetricDatum datum = new MetricDatum()
                .withMetricName(String.format("Status Code: %s", statusCode))
                .withUnit(StandardUnit.Count)
                .withValue(1d)
                .withDimensions(getDimension());

        sendMetrics(datum);
    }

    public void reportHttpStatusCodeCategory(int statusCode) {
        String codeCategory = String.valueOf(statusCode).substring(0, 1) + "xx";
        MetricDatum datum = new MetricDatum()
                .withMetricName(String.format("Status Code Category: %s", codeCategory))
                .withUnit(StandardUnit.Count)
                .withValue(1d)
                .withDimensions(getDimension());

        sendMetrics(datum);
    }


    public void reportHttpStatusCodeCategoryForPath(int statusCode, String requestPath, String requestMethod) {
        String codeCategory = String.valueOf(statusCode).substring(0, 1) + "xx";
        MetricDatum datum = new MetricDatum()
                .withMetricName(String.format("Path %s (%s) Status Code Category: %s", requestPath, requestMethod, codeCategory))
                .withUnit(StandardUnit.Count)
                .withValue(1d)
                .withDimensions(getDimension());

        sendMetrics(datum);
    }

    private Dimension getDimension() {
        return new Dimension()
                    .withName("Environment")
                    .withValue("Production");
    }

    private Collection<MetricDatum> dataCollection = Lists.newArrayList();

    private synchronized void sendMetrics(MetricDatum datum) {
        dataCollection.add(datum);

        int size = dataCollection.size();
        if (size == MAX_DATUM_COLLECTION_SIZE) {
            PutMetricDataRequest request = new PutMetricDataRequest()
                    .withNamespace(serviceName)
                    .withMetricData(dataCollection);

            PutMetricDataResult response = amazonCloudWatch.putMetricData(request);

            dataCollection.clear();
        }
    }
}
