package innmov.common.backend.exception;

/**
 * Created by bruno on 2017-09-28.
 */
public class ExceptionPrinter {

    public String printStackTrace(Throwable exception, boolean filter) {
        exception.getStackTrace();  // Lame workaround for initializing the stack trace which otherwise would show as empty because god knows why
        if (filter) {
            return printFilteredStackTrace(exception);
        } else {
            return printStackTrace(exception);
        }
    }


    private String printFilteredStackTrace(Throwable exception) {
        StringBuilder stringBuilder = new StringBuilder();

        int i = 0;
        for (StackTraceElement element: exception.getStackTrace()) {

            String text = element.toString();
            if (text.toLowerCase().contains("babymanager")
                    || text.toLowerCase().contains("oauth")
                    || i < 5) {
                stringBuilder.append(text + "\n");
            }

            i++;
        }

        return stringBuilder.toString();
    }

    private String printStackTrace(Throwable exception) {
        StringBuilder stringBuilder = new StringBuilder();

        for (StackTraceElement element: exception.getStackTrace()) {
            stringBuilder.append(element.toString() + "\n");
        }

        return stringBuilder.toString();
    }
}
